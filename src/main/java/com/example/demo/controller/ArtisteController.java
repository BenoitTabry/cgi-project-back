package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Artiste;
import com.example.demo.repository.ArtisteRepository;


@RestController
@RequestMapping("/artiste")
public class ArtisteController {
	
	@Autowired
	private ArtisteRepository artisteRepo;

	@GetMapping("/listeArtiste")
	public List<Artiste> findAll() {
		List<Artiste> list = artisteRepo.findAll();
		return list;
	}
	
	@PostMapping("/createArtiste")
	public ResponseEntity<Artiste> createOrUpdate(@RequestBody Artiste artiste) {
		Artiste updated = artisteRepo.save(artiste);
		return new ResponseEntity<Artiste>(updated, new HttpHeaders(), HttpStatus.OK);
	}
}
