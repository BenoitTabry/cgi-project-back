package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Cadre;
import com.example.demo.repository.CadreRepository;

@RestController
@RequestMapping("/cadre")
public class CadreController {
	
	@Autowired
	private CadreRepository cadreRepo;
	
	@GetMapping("/listeCadre")
	public List<Cadre> findAll() {
		List<Cadre> list = cadreRepo.findAll();
		return list; 
	}
	
	@PostMapping("/createCadre")
	public ResponseEntity<Cadre> createOrUpdate(@RequestBody Cadre cadre) {
		Cadre updated = cadreRepo.save(cadre);
		return new ResponseEntity<Cadre>(updated, new HttpHeaders(), HttpStatus.OK);	
	}
 
}
