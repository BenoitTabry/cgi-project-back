package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Commandes;
import com.example.demo.repository.CommandesRepository;

@RestController
@RequestMapping("/commande")
public class CommandesController {
	
	@Autowired
	private CommandesRepository commandeRepo;
	
	@GetMapping("/listeCommande")
	public List<Commandes> findAll() {
		List<Commandes> list = commandeRepo.findAll();
		return list; 
	}
	
	@PostMapping("/createCommande")
	public ResponseEntity<Commandes> createOrUpdate(@RequestBody Commandes commande) {
		Commandes updated = commandeRepo.save(commande);
		return new ResponseEntity<Commandes>(updated, new HttpHeaders(), HttpStatus.OK);	
	}

}
