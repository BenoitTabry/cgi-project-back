package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Commandes_Photographies;
import com.example.demo.repository.Commandes_PhotographiesRepository;;

@RestController
@RequestMapping("/commandePhoto")
public class Commandes_PhotographiesController {
	
	@Autowired
	private Commandes_PhotographiesRepository commande_photoRepo;
	
	@GetMapping("/listeCommandePhoto")
	public List<Commandes_Photographies> findAll() {
		List<Commandes_Photographies> list = commande_photoRepo.findAll();
		return list; 
	}
	
	@PostMapping("/createCommandePhoto")
	public ResponseEntity<Commandes_Photographies> createOrUpdate(@RequestBody Commandes_Photographies commande) {
		Commandes_Photographies updated = commande_photoRepo.save(commande);
		return new ResponseEntity<Commandes_Photographies>(updated, new HttpHeaders(), HttpStatus.OK);	
	}

}
