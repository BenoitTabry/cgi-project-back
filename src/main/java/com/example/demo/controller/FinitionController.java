package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Finition;
import com.example.demo.repository.CommandesRepository;
import com.example.demo.repository.FinitionRepository;

@RestController
@RequestMapping("/finition")
public class FinitionController {
	
	@Autowired
	private FinitionRepository finitionRepo;
	
	@GetMapping("/listeFinition")
	public List<Finition> findAll() {
		List<Finition> list = finitionRepo.findAll();
		return list; 
	}
	
	@PostMapping("/createFinition")
	public ResponseEntity<Finition> createOrUpdate(@RequestBody Finition finition) {
		Finition updated = finitionRepo.save(finition);
		return new ResponseEntity<Finition>(updated, new HttpHeaders(), HttpStatus.OK);	
	}

}
