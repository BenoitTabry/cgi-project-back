package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Format;
import com.example.demo.repository.FinitionRepository;
import com.example.demo.repository.FormatRepository;

@RestController
@RequestMapping("/formats")
public class FormatController {

	
	@Autowired
	private FormatRepository formatRepo;
	
	@GetMapping("/listeFormats")
	public List<Format> findAll() {
		List<Format> list = formatRepo.findAll();
		return list; 
	}
	
	@PostMapping("/createFormats")
	public ResponseEntity<Format> createOrUpdate(@RequestBody Format formats) {
		Format updated = formatRepo.save(formats);
		return new ResponseEntity<Format>(updated, new HttpHeaders(), HttpStatus.OK);	
	}
}
