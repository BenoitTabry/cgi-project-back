package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Orientation;
import com.example.demo.repository.OrientationRepository;

@RestController
@RequestMapping("/orientation")
public class OrientationController {
	
	@Autowired
	private OrientationRepository or;
	
	@GetMapping("/listOrientation")
	public List<Orientation> findAll() {
		List<Orientation> list = or.findAll();
		return list;
	}
	
	@PostMapping("/createOrientation")
	public ResponseEntity<Orientation> createOrUpdate(@RequestBody Orientation o) {
		Orientation updated = or.save(o);
		return new ResponseEntity<Orientation>(updated, new HttpHeaders(), HttpStatus.OK);	
	}

}
