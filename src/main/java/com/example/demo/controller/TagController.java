package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Tag;
import com.example.demo.repository.TagRepository;

@RestController
@RequestMapping("/tag")
public class TagController {
	
	@Autowired
	private TagRepository tr;
	
	@GetMapping("/listTag")
	public List<Tag> findAll() {
		List<Tag> list = tr.findAll();
		return list;
	}
	
	@PostMapping("/createTag")
	public ResponseEntity<Tag> createOrUpdate(@RequestBody Tag tag) {
		Tag updated = tr.save(tag);
		return new ResponseEntity<Tag>(updated, new HttpHeaders(), HttpStatus.OK);	
	}

}
