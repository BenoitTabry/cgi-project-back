package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Theme;
import com.example.demo.repository.TagRepository;
import com.example.demo.repository.ThemeRepository;

@RestController
@RequestMapping("/themes")
public class ThemesController {
	
	@Autowired
	private ThemeRepository tr;
	
	@GetMapping("/listThemes")
	public List<Theme> findAll() {
		List<Theme> list = tr.findAll();
		return list;
	}
	
	@PostMapping("/createThemes")
	public ResponseEntity<Theme> createOrUpdate(@RequestBody Theme themes) {
		Theme updated = tr.save(themes);
		return new ResponseEntity<Theme>(updated, new HttpHeaders(), HttpStatus.OK);	
	}

}
