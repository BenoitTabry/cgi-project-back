package com.example.demo.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "authentification")
public class Authentification {
	@GeneratedValue
	@Id
	private int id_auth;
	private String email;
	private String password;

	@ManyToMany(mappedBy = "authentification")
	private Collection<Utilisateur> utilisateurAuth;

	public int getId_auth() {
		return id_auth;
	}

	public void setId_auth(int id_auth) {
		this.id_auth = id_auth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<Utilisateur> getUtilisateurAuth() {
		return utilisateurAuth;
	}

	public void setUtilisateurAuth(Collection<Utilisateur> utilisateurAuth) {
		this.utilisateurAuth = utilisateurAuth;
	}

	public Authentification(int id_auth, String email, String password, Collection<Utilisateur> utilisateurAuth) {

		this.id_auth = id_auth;
		this.email = email;
		this.password = password;
		this.utilisateurAuth = utilisateurAuth;
	}

	public Authentification() {

	}

//	@Override
//	public String toString() {
//		return "Authentification [id_auth=" + id_auth + ", email=" + email + ", password=" + password
//				+ ", utilisateurAuth=" + utilisateurAuth + "]";
//	}

}
