package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "commandes")
public class Commandes {

	@GeneratedValue
	@Id
	private int id_Commande;

	@ManyToOne
	@JoinColumn(name = "id_utilisateur")
	private Utilisateur utilisateurCommande;

	private Date date_Achat;


	private float prix_Final;

	public int getId_Commande() {
		return id_Commande;
	}

	public void setId_Commande(int id_Commande) {
		this.id_Commande = id_Commande;
	}

	public Utilisateur getUtilisateurCommande() {
		return utilisateurCommande;
	}

	public void setUtilisateurCommande(Utilisateur utilisateurCommande) {
		this.utilisateurCommande = utilisateurCommande;
	}

	public Date getDate_Achat() {
		return date_Achat;
	}

	public void setDate_Achat(Date date_Achat) {
		this.date_Achat = date_Achat;
	}


	public float getPrix_Final() {
		return prix_Final;
	}

	public void setPrix_Final(float prix_Final) {
		this.prix_Final = prix_Final;
	}

//	@Override
//	public String toString() {
//		return "Commandes [id_Commande=" + id_Commande + ", utilisateurCommande=" + utilisateurCommande
//				+ ", date_Achat=" + date_Achat + ", role=" + role + ", prix_Final=" + prix_Final + "]";
//	}

	public Commandes(int id_Commande, Utilisateur utilisateurCommande, Date date_Achat, float prix_Final) {

		this.id_Commande = id_Commande;
		this.utilisateurCommande = utilisateurCommande;
		this.date_Achat = date_Achat;
		this.prix_Final = prix_Final;
	}

	public Commandes() {

	}

}
