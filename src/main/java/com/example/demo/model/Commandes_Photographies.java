package com.example.demo.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "commandes_photographies")
public class Commandes_Photographies {
	@GeneratedValue
	@Id
	private int id_Commande_photographie;
	private int quantite;

	@ManyToOne
	@JoinColumn(name = "id_commande")
	private Commandes commandes;

	@ManyToOne
	@JoinColumn(name = "id_photo")
	private Photographies photoCommande;

	@ManyToOne
	@JoinColumn(name = "id_cadre")
	private Cadre cadre;

	@ManyToOne
	@JoinColumn(name = "id_finition")
	private Finition finition;

	@ManyToOne
	@JoinColumn(name = "id_format")
	private Format formats;

	public int getId_Commande_photographie() {
		return id_Commande_photographie;
	}

	public void setId_Commande_photographie(int id_Commande_photographie) {
		this.id_Commande_photographie = id_Commande_photographie;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public Commandes getCommandes() {
		return commandes;
	}

	public void setCommandes(Commandes commandes) {
		this.commandes = commandes;
	}

	public Photographies getPhotoCommande() {
		return photoCommande;
	}

	public void setPhotoCommande(Photographies photoCommande) {
		this.photoCommande = photoCommande;
	}

	public Cadre getCadre() {
		return cadre;
	}

	public void setCadre(Cadre cadre) {
		this.cadre = cadre;
	}

	public Finition getFinition() {
		return finition;
	}

	public void setFinition(Finition finition) {
		this.finition = finition;
	}

	public Format getFormats() {
		return formats;
	}

	public void setFormats(Format formats) {
		this.formats = formats;
	}

//	@Override
//	public String toString() {
//		return "Commandes_Photographies [id_Commande_photographie=" + id_Commande_photographie + ", quantite="
//				+ quantite + ", commandes=" + commandes + ", photoCommande=" + photoCommande + ", cadre=" + cadre
//				+ ", finition=" + finition + ", formats=" + formats + "]";
//	}

	public Commandes_Photographies(int id_Commande_photographie, int quantite, Commandes commandes,
			Photographies photoCommande, Cadre cadre, Finition finition, Format formats) {

		this.id_Commande_photographie = id_Commande_photographie;
		this.quantite = quantite;
		this.commandes = commandes;
		this.photoCommande = photoCommande;
		this.cadre = cadre;
		this.finition = finition;
		this.formats = formats;
	}

	public Commandes_Photographies() {

	}

}
