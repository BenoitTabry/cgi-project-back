package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "decorations")
public class Decorations {
	@GeneratedValue
	@Id
	private int id_deco;
	private String image_Url_Deco;

	@ManyToOne
	@JoinColumn(name = "id_photo")
	private Photographies photoDeco;

	public int getId_deco() {
		return id_deco;
	}

	public void setId_deco(int id_deco) {
		this.id_deco = id_deco;
	}

	public String getImage_Url_Deco() {
		return image_Url_Deco;
	}

	public void setImage_Url_Deco(String image_Url_Deco) {
		this.image_Url_Deco = image_Url_Deco;
	}

	public Photographies getPhotoDeco() {
		return photoDeco;
	}

	public void setPhotoDeco(Photographies photoDeco) {
		this.photoDeco = photoDeco;
	}

//	@Override
//	public String toString() {
//		return "Decorations [id_deco=" + id_deco + ", image_Url_Deco=" + image_Url_Deco + ", photoDeco=" + photoDeco
//				+ "]";
//	}

	public Decorations(int id_deco, String image_Url_Deco, Photographies photoDeco) {

		this.id_deco = id_deco;
		this.image_Url_Deco = image_Url_Deco;
		this.photoDeco = photoDeco;
	}

	public Decorations() {

	}

}
