package com.example.demo.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "finition")
public class Finition {
	@GeneratedValue
	@Id
	private int id_Finition;

	private String finition;
	private String description;
	private String image_Url;

	@OneToMany(mappedBy = "finition")
	private Collection<Commandes_Photographies> commandePhoto;

	public int getId_Finition() {
		return id_Finition;
	}

	public void setId_Finition(int id_Finition) {
		this.id_Finition = id_Finition;
	}

	public String getFinition() {
		return finition;
	}

	public void setFinition(String finition) {
		this.finition = finition;
	}

	public Collection<Commandes_Photographies> getCommandePhoto() {
		return commandePhoto;
	}

	public void setCommandePhoto(Collection<Commandes_Photographies> commandePhoto) {
		this.commandePhoto = commandePhoto;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage_Url() {
		return image_Url;
	}

	public void setImage_Url(String image_Url) {
		this.image_Url = image_Url;
	}

//	@Override
//	public String toString() {
//		return "Finition [id_Finition=" + id_Finition + ", finition=" + finition + ", description=" + description
//				+ ", image_Url=" + image_Url + ", commandePhoto=" + commandePhoto + "]";
//	}

	public Finition(int id_Finition, String finition, String description, String image_Url,
			Collection<Commandes_Photographies> commandePhoto) {

		this.id_Finition = id_Finition;
		this.finition = finition;
		this.description = description;
		this.image_Url = image_Url;
		this.commandePhoto = commandePhoto;
	}

	public Finition() {

	}

}
