package com.example.demo.model;

import java.sql.Date;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "photographies")
public class Photographies {

	@GeneratedValue
	@Id
	private int id_Photo;
	private float prix_Base;
	private String titre_Photo;
	private String image_Url;
	private String image_Url2;
	private int nb_Tirages;
	private int nb_Vente;
	private int stock;
	private boolean isDIspo_Galery;
	private boolean isDispo_Livraison;
	private Date date_Mise_en_vente;

	
	@ManyToOne
	@JoinColumn(name = "id_artist")
	private Artiste artiste;

	@OneToMany(mappedBy = "photoCommande")
	private Collection<Commandes_Photographies> commandePhoto;

	@OneToMany(mappedBy = "photoTag")
	private Collection<Tag> tag;

	@ManyToOne
	@JoinColumn(name = "id_orientation")
	private Orientation orientation;

	@ManyToMany
	@JoinTable(name = "formats_photographies", joinColumns = @JoinColumn(name = "id_photo"), inverseJoinColumns = @JoinColumn(name = "id_format"))
	private Collection<Format> format;

	@ManyToMany
	@JoinTable(name = "themes_photographies", joinColumns = @JoinColumn(name = "id_photo"), inverseJoinColumns = @JoinColumn(name = "id_theme"))
	private Collection<Theme> theme;

	@OneToMany(mappedBy = "photoDeco")
	private Collection<Decorations> decorations;

	public int getId_Photo() {
		return id_Photo;
	}

	public void setId_Photo(int id_Photo) {
		this.id_Photo = id_Photo;
	}

	public float getPrix_Base() {
		return prix_Base;
	}

	public void setPrix_Base(float prix_Base) {
		this.prix_Base = prix_Base;
	}

	public String getTitre_Photo() {
		return titre_Photo;
	}

	public void setTitre_Photo(String titre_Photo) {
		this.titre_Photo = titre_Photo;
	}

	public String getImage_Url() {
		return image_Url;
	}

	public void setImage_Url(String image_Url) {
		this.image_Url = image_Url;
	}

	public String getImage_Url2() {
		return image_Url2;
	}

	public void setImage_Url2(String image_Url2) {
		this.image_Url2 = image_Url2;
	}

	public int getNb_Tirages() {
		return nb_Tirages;
	}

	public void setNb_Tirages(int nb_Tirages) {
		this.nb_Tirages = nb_Tirages;
	}

	public int getNb_Vente() {
		return nb_Vente;
	}

	public void setNb_Vente(int nb_Vente) {
		this.nb_Vente = nb_Vente;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public boolean isDIspo_Galery() {
		return isDIspo_Galery;
	}

	public void setDIspo_Galery(boolean isDIspo_Galery) {
		this.isDIspo_Galery = isDIspo_Galery;
	}

	public boolean isDispo_Livraison() {
		return isDispo_Livraison;
	}

	public void setDispo_Livraison(boolean isDispo_Livraison) {
		this.isDispo_Livraison = isDispo_Livraison;
	}

	public Date getDate_Mise_en_vente() {
		return date_Mise_en_vente;
	}

	public void setDate_Mise_en_vente(Date date_Mise_en_vente) {
		this.date_Mise_en_vente = date_Mise_en_vente;
	}

	public Artiste getArtiste() {
		return artiste;
	}

	public void setArtiste(Artiste artiste) {
		this.artiste = artiste;
	}

	public Collection<Commandes_Photographies> getCommandePhoto() {
		return commandePhoto;
	}

	public void setCommandePhoto(Collection<Commandes_Photographies> commandePhoto) {
		this.commandePhoto = commandePhoto;
	}

	public Collection<Tag> getTag() {
		return tag;
	}

	public void setTag(Collection<Tag> tag) {
		this.tag = tag;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public Collection<Format> getFormat() {
		return format;
	}

	public void setFormat(Collection<Format> format) {
		this.format = format;
	}

	public Collection<Theme> getTheme() {
		return theme;
	}

	public void setTheme(Collection<Theme> theme) {
		this.theme = theme;
	}

	public Collection<Decorations> getDecorations() {
		return decorations;
	}

	public void setDecorations(Collection<Decorations> decorations) {
		this.decorations = decorations;
	}

	public Photographies(int id_Photo, float prix_Base, String titre_Photo, String image_Url, String image_Url2,
			int nb_Tirages, int nb_Vente, int stock, boolean isDIspo_Galery, boolean isDispo_Livraison,
			Date date_Mise_en_vente, Artiste artiste, Collection<Commandes_Photographies> commandePhoto,
			Collection<Tag> tag, Orientation orientation, Collection<Format> format, Collection<Theme> theme,
			Collection<Decorations> decorations) {

		this.id_Photo = id_Photo;
		this.prix_Base = prix_Base;
		this.titre_Photo = titre_Photo;
		this.image_Url = image_Url;
		this.image_Url2 = image_Url2;
		this.nb_Tirages = nb_Tirages;
		this.nb_Vente = nb_Vente;
		this.stock = stock;
		this.isDIspo_Galery = isDIspo_Galery;
		this.isDispo_Livraison = isDispo_Livraison;
		this.date_Mise_en_vente = date_Mise_en_vente;
		this.artiste = artiste;
		this.commandePhoto = commandePhoto;
		this.tag = tag;
		this.orientation = orientation;
		this.format = format;
		this.theme = theme;
		this.decorations = decorations;
	}

	public Photographies() {

	}

//	@Override
//	public String toString() {
//		return "Photographies [id_Photo=" + id_Photo + ", prix_Base=" + prix_Base + ", titre_Photo=" + titre_Photo
//				+ ", image_Url=" + image_Url + ", image_Url2=" + image_Url2 + ", nb_Tirages=" + nb_Tirages
//				+ ", nb_Vente=" + nb_Vente + ", stock=" + stock + ", isDIspo_Galery=" + isDIspo_Galery
//				+ ", isDispo_Livraison=" + isDispo_Livraison + ", date_Mise_en_vente=" + date_Mise_en_vente
//				+ ", artiste=" + artiste + ", commandePhoto=" + commandePhoto + ", tag=" + tag + ", orientation="
//				+ orientation + ", format=" + format + ", theme=" + theme + ", decorations=" + decorations + "]";
//	}

}
