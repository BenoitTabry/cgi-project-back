package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "reseaux_sociaux")
public class Reseaux_Sociaux {

	@GeneratedValue
	@Id
	private int id_Reseaux_Sociaux;

	private String facebook_Url;
	private String twitter_Url;
	private String pinteres_Url;

	@JsonIgnore
	@OneToOne(mappedBy = "reseauxSociaux")
	private Artiste artiste;

	public int getId_Reseaux_Sociaux() {
		return id_Reseaux_Sociaux;
	}

	public void setId_Reseaux_Sociaux(int id_Reseaux_Sociaux) {
		this.id_Reseaux_Sociaux = id_Reseaux_Sociaux;
	}

	public String getFacebook_Url() {
		return facebook_Url;
	}

	public void setFacebook_Url(String facebook_Url) {
		this.facebook_Url = facebook_Url;
	}

	public String getTwitter_Url() {
		return twitter_Url;
	}

	public void setTwitter_Url(String twitter_Url) {
		this.twitter_Url = twitter_Url;
	}

	public String getPinteres_Url() {
		return pinteres_Url;
	}

	public void setPinteres_Url(String pinteres_Url) {
		this.pinteres_Url = pinteres_Url;
	}

	public Artiste getArtiste() {
		return artiste;
	}

	public void setArtiste(Artiste artiste) {
		this.artiste = artiste;
	}

//	@Override
//	public String toString() {
//		return "Reseaux_Sociaux [id_Reseaux_Sociaux=" + id_Reseaux_Sociaux + ", facebook_Url=" + facebook_Url
//				+ ", twitter_Url=" + twitter_Url + ", pinteres_Url=" + pinteres_Url + ", artiste=" + artiste + "]";
//	}

	public Reseaux_Sociaux(int id_Reseaux_Sociaux, String facebook_Url, String twitter_Url, String pinteres_Url,
			Artiste artiste) {

		this.id_Reseaux_Sociaux = id_Reseaux_Sociaux;
		this.facebook_Url = facebook_Url;
		this.twitter_Url = twitter_Url;
		this.pinteres_Url = pinteres_Url;
		this.artiste = artiste;
	}

	public Reseaux_Sociaux() {

	}

}
