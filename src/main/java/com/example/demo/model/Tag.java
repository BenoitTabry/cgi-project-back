package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tag")
public class Tag {
	@GeneratedValue
	@Id
	private int id_tag;

	private String tag;

	@ManyToOne
	@JoinColumn(name = "id_photo")
	private Photographies photoTag;

	public int getId_tag() {
		return id_tag;
	}

	public void setId_tag(int id_tag) {
		this.id_tag = id_tag;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Photographies getPhotoTag() {
		return photoTag;
	}

	public void setPhotoTag(Photographies photoTag) {
		this.photoTag = photoTag;
	}

//	@Override
//	public String toString() {
//		return "Tag [id_tag=" + id_tag + ", tag=" + tag + ", photoTag=" + photoTag + "]";
//	}

	public Tag(int id_tag, String tag, Photographies photoTag) {

		this.id_tag = id_tag;
		this.tag = tag;
		this.photoTag = photoTag;
	}

	public Tag() {

	}

}
