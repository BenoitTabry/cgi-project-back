package com.example.demo.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "utilisateur")
public class Utilisateur {
	@GeneratedValue
	@Id
	private int id_utilisateur; //

	private String civilite; //
	private String nom; //
	private String prenom; //
	private String email; //
	private String telephone;//
	private String password; //

	@JsonIgnore
	@OneToMany(mappedBy = "utilisateurAdresse") //
	private Collection<Adresse> adresse;


	@OneToMany(mappedBy = "utilisateurCommande") //
	private Collection<Commandes> commandes;

	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "authentification_utilisateur", joinColumns = @JoinColumn(name = "id_utilisateur"), inverseJoinColumns = @JoinColumn(name = "id_authentification"))
	private Collection<Authentification> authentification;

	public int getId_utilisateur() {
		return id_utilisateur;
	}

	public void setId_utilisateur(int id_utilisateur) {
		this.id_utilisateur = id_utilisateur;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<Adresse> getAdresse() {
		return adresse;
	}

	public void setAdresse(Collection<Adresse> adresse) {
		this.adresse = adresse;
	}


	public Collection<Commandes> getCommandes() {
		return commandes;
	}

	public void setCommandes(Collection<Commandes> commandes) {
		this.commandes = commandes;
	}

	public Collection<Authentification> getAuthentification() {
		return authentification;
	}

	public void setAuthentification(Collection<Authentification> authentification) {
		this.authentification = authentification;
	}

//	@Override
//	public String toString() {
//		return "Utilisateur [id_utilisateur=" + id_utilisateur + ", civilite=" + civilite + ", nom=" + nom + ", prenom="
//				+ prenom + ", email=" + email + ", telephone=" + telephone + ", password=" + password + ", adresse="
//				+ adresse + ", role=" + role + ", commandes=" + commandes + ", authentification=" + authentification
//				+ "]";
//	}

	public Utilisateur(int id_utilisateur, String civilite, String nom, String prenom, String email, String telephone,
			String password, Collection<Adresse> adresse, Collection<Commandes> commandes,
			Collection<Authentification> authentification) {

		this.id_utilisateur = id_utilisateur;
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.password = password;
		this.adresse = adresse;
		this.commandes = commandes;
		this.authentification = authentification;
	}

	public Utilisateur() {

	}

}
