package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Commandes;

@Repository
public interface CommandesRepository extends JpaRepository<Commandes, Integer>{

}
