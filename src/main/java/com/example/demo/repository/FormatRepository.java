package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Format;

@Repository
public interface FormatRepository extends JpaRepository<Format, Integer>{

}
