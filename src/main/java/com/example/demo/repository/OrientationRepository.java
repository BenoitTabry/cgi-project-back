package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Orientation;

@Repository
public interface OrientationRepository extends JpaRepository<Orientation, Integer>{

}
