package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Photographies;

@Repository
public interface PhotographiesRepository extends JpaRepository<Photographies, Integer>{
	@Query("SELECT p FROM Photographies as p order by date_mise_en_vente desc")
	List<Photographies> findRecentPhotographies();
	// Pageable firstpage
	
	@Query("select p from Photographies as p order by nb_vente desc")
	List<Photographies> findBestPhotographies();
	
	@Query("select p from Photographies as p where id_photo=2")
	Photographies findPhotographiesById();
	
	//List<Photographies> 
}
