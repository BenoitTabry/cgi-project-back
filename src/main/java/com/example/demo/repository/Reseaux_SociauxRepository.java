package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Reseaux_Sociaux;

@Repository
public interface Reseaux_SociauxRepository extends JpaRepository<Reseaux_Sociaux, Integer>{

}
