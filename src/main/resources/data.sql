
INSERT INTO `kartina`.`adresse`
(`id_adresse`,
`code_postale`,
`is_active`,
`numero`,
`pays`,
`rue`,
`ville`,
`id_utilisateur`)
VALUES
(1,
"75000",
true,
83,
"France",
"Jean Jaures",
"Paris",
1),
(2,
"54000",
true,
45,
"France",
"Rue Stanislas",
"Nancy",
1),
(3,
"35000",
true,
45,
"France",
"Villejean",
"Rennes",
2);


SELECT * FROM kartina.utilisateur;INSERT INTO `kartina`.`utilisateur`
(`id_utilisateur`,
`civilite`,
`email`,
`nom`,
`password`,
`prenom`,
`telephone`)
VALUES
(1,
"Mr",
"contact@test.com",
"Dupond",
"1234",
"Toto",
"0645324232"),
(2,
"Mme",
"contact@test.com",
"Suc",
"1234",
"Michelle",
"0677777777");


INSERT INTO `kartina`.`artiste`
(`id_artist`,
`nom`,
`prenom`,
`presentation`,
`id_reseaux_sociaux`)
VALUES
(1,"Cadir","Hasret","Big Boss Beau Goss",1),
(2,"Jean","Marie","Eazy E boyz",2),
(3,"Jacque","Bruce","Sub i sub",3),
(4,"Jacque","Bruce","Sub i sub",4),
(5,"Jacque","Bruce","Sub i sub",5),
(6,"Jacque","Bruce","Sub i sub",6),
(7,"Jacque","Bruce","Sub i sub",7),
(8,"Jacque","Bruce","Sub i sub",8),
(9,"Jacque","Bruce","Sub i sub",9);


INSERT INTO `kartina`.`authentification`
(`id_auth`,
`email`,
`password`)
VALUES
(1,
"jean.marie@gmail.com",
"azerty"),
(2,
"marie.jean@gmail.com",
"aaa");
SELECT * FROM kartina.authentification;

INSERT INTO `kartina`.`authentification_utilisateur`
(`id_utilisateur`,
`id_authentification`)
VALUES
(1,
1),
(2,
2);

INSERT INTO `kartina`.`orientation`
(`id_orientation`,
`orientation`)
VALUES
(1,
"Portrait"),
(2,
"Paysage"),
(3,
"Carré"),
(4,
"Panoramique");

INSERT INTO `kartina`.`formats`
(`id_format`,
`description`,
`format`,
`image_url`,
`is_format_dispo`,
`stock_format`)
VALUES
(1,
"Tirage encadré, édition limitée à","Classique", " ",True, 450),
(2,
"Photographie montée sur aluminium, édition limitée","Grand", " ",True,450),
(3,
"Photographie montée sur aluminium, édition limitée","Géant", " ",True,450),
(4,
"Photographie montée sur aluminium, édition limitée", "Collector"," ",True,450);

INSERT INTO `kartina`.`reseaux_sociaux`
(`id_reseaux_sociaux`,
`facebook_url`,
`pinteres_url`,
`twitter_url`)
VALUES
(1,"","",""),
(2,"","",""),
(3,"","",""),
(4,"","",""),
(5,"","",""),
(6,"","",""),
(7,"","",""),
(8,"","",""),
(9,"","",""),
(10,"","","");


INSERT INTO `kartina`.`theme`
(`id_theme`,
`theme`)
VALUES
(1,
"Femme"),
(2,
"Nature"),
(3,
"Noir et Blanc"),
(4,
"Paysage"),
(5,
"Urban");

INSERT INTO `kartina`.`finition`
(`id_finition`,
`finition`, `description`)
VALUES
(1,
"Support aluminium", "Tirage contrecollé sur support aluminium"),
(2,
"Support aluminium avec verre acrylique",
"Tirage contrecollé sur support aluminium avec finition protectrice en verre acrylique accentuant les contrastes et les couleurs"),
(3,
"Tirage sur papier photo", "Tirage sur papier photo expédié roulé, à accrocher ou à encadrer"),
(4,
"Blackout", "Passe-partout noir avec liseret blanc"),
(5,
"Artshot", "Passe-partout blanc");

INSERT INTO `kartina`.`cadre`
(`id_cadre`,
`cadre`,`description`)
VALUES
(1,
"Sans encadrement",
"Tirage contrecollé sur support aluminium"),
(2,
"Encadrement noir satin",
"Caisse américaine noire satinée assemblée à la main, apportant un effet de luminosité et de profondeur"),
(3,
"Encadrement blanc satin",
"Caisse américaine blanche satinée assemblée à la main, apportant un effet de luminosité et de profondeur"),
(4,
"Encadrement noyer",
"Caisse américaine en noyer massif assemblée à la main, apportant un effet de luminosité et de profondeur"),
(5,
"Encadrement chêne",
"Caisse américaine en chêne massif assemblée à la main, apportant un effet de luminosité et de profondeur"),
(6,
"Aluminium noir",
"Format 50x40cm"),
(7,
"Bois blanc",
"Format 50x40cm"),
(8,
"Acajou mat",
"Format 50x40cm"),
(9,
"Aluminium brossé",
"Format 50x40cm");

INSERT INTO `kartina`.`photographies`
(`id_photo`,
`date_mise_en_vente`,
`image_url`,
`image_url2`,
`isdispo_galery`,
`is_dispo_livraison`,
`nb_tirages`,
`nb_vente`,
`prix_base`,
`stock`,
`titre_photo`,
`id_artist`,
`id_orientation`)
VALUES
(1,'2015-06-08',"abstract1.jpg","abstract1_salon.jpg",True,True,3000,100,150,2800,"Peinture abstraict magiciens",2,3),
(2,'2010-04-11',"alley.jpg","alley_salon.jpg",True,True,4500,250,350,4000,"Paysage centre ville Cambridge",1,2),
(3,'2007-10-2',"alpaca.jpg","alpaca_salon.jpg",True,True,5000,100,480,3500,"Alpaca de Pérou",3,2),
(4,'2005-08-22',"giraphe.jpg","giraphe_salon.jpg",True,True,2500,25,150,2000,"Giraphe d'Afrique",4,2),
(5,'2000-02-19',"chien.jpg","chien_salon.jpg",True,True,2500,300,100,2000,"Chien domestique",5,1),
(6,'2002-07-12',"autumn.jpg","autumn_salon.jpg",True,True,3000,25,80,2800,"L'automne à son retour",6,2),
(7,'2012-05-11',"deer.jpg","deer_salon.jpg",True,True,1500,25,180,1200,"Cerf qui est volant",7,2),
(8,'2001-10-30',"harvesting.jpg","harvesting_salon.jpg",True,True,1000,200,280,800,"Le jour de la récolte",8,2),
(9,'2003-08-11',"old-town.jpg","old-town_salon.jpg",True,True,1000,200,300,800,"Ancienne ville",9,2),
(10,'1999-07-01',"painting.jpg","painting_salon.jpg",True,True,2800,120,380,2800,"La femme multitâche",9,1);






